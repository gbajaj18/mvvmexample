//
//  ViewController.swift
//  MVVMExample
//
//  Created by gaurav on 27/12/18.
//  Copyright © 2018 clavax. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    //MARK: - Global Variable
    let objVM = PhotoViewModel()
    //    lazy var objVM: PhotoViewModel = {
    //        return PhotoViewModel()
    //    }()
    
    var photoArray = [Photo]()
    
    //MARK: - IBOutlet
    @IBOutlet weak var tblPhoto: UITableView!
    
    //MARK: - View Load Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Basic Setup
        self.viewSetup()
        
        // Set Call back for Clouser.
        objVM.updateUI = { (dataArray) in
            
            self.photoArray = dataArray
            
            DispatchQueue.main.async {
                self.tblPhoto.reloadData()
            }
            
        }
        
        // Call Api Request
        objVM.fetchData()
        
    }
    
    //MARK: - Helper Method
    func viewSetup() {
        self.navigationItem.title = "PhotoList"
        tblPhoto.rowHeight = UITableView.automaticDimension
    }
    
    
}

extension HomeVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.photoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "photoTblCell", for: indexPath) as! photoTblCell
        let objPhoto = photoArray[indexPath.row]
        
        cell.updateCell(objPhoto: objPhoto)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    
}
