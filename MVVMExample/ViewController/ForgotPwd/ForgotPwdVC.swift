//
//  ForgotPwdViewController.swift
//  MVVMExample
//
//  Created by gaurav on 03/01/19.
//  Copyright © 2019 clavax. All rights reserved.
//

import UIKit

class ForgotPwdVC: UIViewController, UITextFieldDelegate {

    //MARK: - Global Variable
    var objRegVM = OnBoardingVM()
    
    //MARK: - IBOutlet
    @IBOutlet weak var tfEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Forgot Password"

        // Do any additional setup after loading the view.
    }
    
    //MARK: - TextField Delgate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

    //MARK: - Button Click
    @IBAction func onSubmitBtnClick(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        // Pass Value and Return Validation State.
        let validateState = objRegVM.updateValueInModelRegistration(updateString:[tfEmail.text ?? ""], screenType: ScreenType.forgotPassword)
        
        switch validateState {
            
        case .invalid(let alertMessage):
            UtilityClass.showAlertView(title: key.k_Alert, message: alertMessage, in: self)
            
        case .valid:
            print("Valid")
            
            // Call api or extra work
            
            
        }
        
    }
    
    

}
