//
//  RegistrationViewController.swift
//  MVVMExample
//
//  Created by Gaurav Bajaj on 02/01/19.
//  Copyright © 2019 clavax. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, UITextFieldDelegate {

    //MARK: - Global Variable
    var objRegVM = OnBoardingVM()
    
    //MARK: - IBOutlet
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!

    
    //MARK: - View Load Method
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - TextField Delgate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    //MARK: - Button Click
    @IBAction func onSubmitBtnClick(_ sender: UIButton) {
    
    self.view.endEditing(true)
    
    // Pass Value and Return Validation State.
    let validateState = objRegVM.updateValueInModelRegistration(updateString:[tfFirstName.text ?? "", tfLastName.text ?? "", tfEmail.text ?? "", tfPassword.text ?? ""], screenType: ScreenType.registration)
    
    switch validateState {
        case .invalid(let alertMessage):
            UtilityClass.showAlertView(title: key.k_Alert, message: alertMessage, in: self)
        case .valid:
            print("Valid")
        }
    }
    
}
