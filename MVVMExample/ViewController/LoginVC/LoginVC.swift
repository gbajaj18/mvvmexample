//
//  LoginVC.swift
//  MVVMExample
//
//  Created by gaurav on 03/01/19.
//  Copyright © 2019 clavax. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate {

    //MARK: - Global Variable
    
    var objRegVM = OnBoardingVM()
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    //MARK: - View Load Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    //MARK: - TextField Delgate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    //MARK: - Button Click
    
    @IBAction func onLoginBtnClick(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        // Pass Value and Return Validation State.
        
        let validateState = objRegVM.updateValueInModelRegistration(updateString:[tfEmail.text ?? "", tfPassword.text ?? ""], screenType: ScreenType.login)
        
        switch validateState {
            
        case .invalid(let alertMessage):
            UtilityClass.showAlertView(title: key.k_Alert, message: alertMessage, in: self)
            
        case .valid:
            print("Valid")
            
            // Call api or extra work
            
            self.goToHomePage()
            
        }
        
    }
    
    @IBAction func onRegistarionBtnClick(_ sender: UIButton) {
        
        let objRegistarionVC = UtilityClass.findStoryBoard(storyboardName: key.k_SB_Onboarding).instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        self.navigationController?.pushViewController(objRegistarionVC, animated: true)
        
    }
    
    
    @IBAction func onForgotPwdBtnClick(_ sender: UIButton) {
        
        let objForgotPwdVC =  UtilityClass.findStoryBoard(storyboardName: key.k_SB_Onboarding).instantiateViewController(withIdentifier: "ForgotPwdVC") as! ForgotPwdVC
        self.navigationController?.pushViewController(objForgotPwdVC, animated: true)
        
    }
    
    //MARK: - Helper Method
    
    func goToHomePage(){
        
        let objHomeVC = UtilityClass.findStoryBoard(storyboardName: key.k_SB_Dashboard).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(objHomeVC, animated: true)
        
    }
    
}
