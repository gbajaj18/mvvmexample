//
//  RegisterModel.swift
//  MVVMExample
//
//  Created by Gaurav Bajaj on 03/01/19.
//  Copyright © 2019 clavax. All rights reserved.
//

import Foundation

struct textFieldStruct {
    
    var firstname : String = ""
    var lastName : String = ""
    var email : String = ""
    var password : String = ""
    
    
    // For Registration
    init(firstname : String, lastName: String, email: String, password: String) {
        self.firstname = firstname
        self.lastName = lastName
        self.email = email
        self.password = password
    }
    
    // For Forgot Password
    init(email : String) {
        self.email = email
    }
    
    // For Login
    init(email : String, password : String) {
        self.email = email
        self.password = password
    }
    
    // For Reset Password.
    init(password : String) {
        self.password = password
    }
    
}


