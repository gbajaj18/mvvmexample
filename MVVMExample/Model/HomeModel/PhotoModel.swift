//
//  PhotoModel.swift
//  MVVMExample
//
//  Created by gaurav on 02/01/19.
//  Copyright © 2019 clavax. All rights reserved.
//

import Foundation

struct PartsPhoto : Codable {
    let photos : [Photo]
}

struct Photo : Codable {
    
    var name: String
    var description: String
    var image_url: String
    
    //    init(name : String, descritpion: String, imageURL: String) {
    //        self.name = name
    //        self.description = descritpion
    //        self.imageURL = imageURL
    //    }
    
}
