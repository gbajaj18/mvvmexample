//
//  photoTblCell.swift
//  MVVMExample
//
//  Created by gaurav on 27/12/18.
//  Copyright © 2018 clavax. All rights reserved.
//

import UIKit
import SDWebImage


class photoTblCell: UITableViewCell {
    
    @IBOutlet weak var photoImgView: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCell(objPhoto : Photo) {
        lblName.text = objPhoto.name
        lblDescription.text = objPhoto.description
        photoImgView.sd_setImage(with: URL(string: objPhoto.image_url), completed: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
