//
//  RegisterViewModel.swift
//  MVVMExample
//
//  Created by Gaurav Bajaj on 02/01/19.
//  Copyright © 2019 clavax. All rights reserved.
//

import Foundation

//MARK: - Enum Declartion
enum ValidationState {
    case invalid(String)
    case valid
}

enum ScreenType : Int {
    case login
    case registration
    case forgotPassword
    case resetPassword
}

class OnBoardingVM {
    
    //MARK: - Global Variable
    public var objtextFieldStruct : textFieldStruct!
    
    
    //MARK: - Validation Method
    func validate(screenType : ScreenType) -> ValidationState {
        
        switch screenType {
        case .registration:
           
            if objtextFieldStruct.firstname.trim().count <= 0 {
                return .invalid(key.k_Msg_FirstName)
            }
            else if objtextFieldStruct.lastName.trim().count <= 0 {
                return .invalid(key.k_Msg_LastName)
            }
            else if objtextFieldStruct.email.trim().count <= 0 || !(objtextFieldStruct.email.isValidEmail())
            {
                return .invalid(key.k_Msg_Email)
            }
            else if objtextFieldStruct.password == "" {
                return .invalid(key.k_Msg_Password)
            }
            else if objtextFieldStruct.password.count < 8 || objtextFieldStruct.password.count > 20 {
                return .invalid(key.k_Msg_PasswordRange)
            }
            
            return .valid
            
        case .login:
            
            if objtextFieldStruct.email.trim().count <= 0 || !(objtextFieldStruct.email.isValidEmail()) {
                return .invalid(key.k_Msg_Email)
            }
            else if objtextFieldStruct.password == "" {
                return .invalid(key.k_Msg_Password)
            }
             
            return .valid
            
        case .forgotPassword:
            
            if objtextFieldStruct.email.trim().count <= 0 || !(objtextFieldStruct.email.isValidEmail()) {
                return .invalid(key.k_Msg_Email)
            }
            
            return .valid
            
        case .resetPassword:
            
            if objtextFieldStruct.password == "" {
                return .invalid(key.k_Msg_Password)
            }
            
            return .valid
        
        }
        
    }
    
    //MARK: - Data Updation Method
    func updateValueInModelRegistration(updateString : [String], screenType : ScreenType) -> ValidationState {
        
        switch screenType {
        case .registration:
            objtextFieldStruct = textFieldStruct(firstname: updateString[0], lastName: updateString[1], email: updateString[2], password: updateString[3])
            
        case .login:
            objtextFieldStruct = textFieldStruct(email:updateString[0] , password: updateString[1])
            
        case .forgotPassword:
            objtextFieldStruct = textFieldStruct(email: updateString[0])
            
        case .resetPassword:
            objtextFieldStruct = textFieldStruct(password: updateString[0])
        }
        
        return self.validate(screenType: screenType)
       
    }
    
    //MARK: - Network Request Method
    func callApi(inputDict : [String : Any], methodName : String){
        
        
        
        
    }
    
}
