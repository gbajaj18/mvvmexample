import Foundation

class PhotoViewModel {
    
    // Set UI Update Closure
    var updateUI : (([Photo])->())?
    
    // Set Observer for Array value update.
    private var photoVMArray  = [Photo]() {
        didSet {
            self.updateUI?(photoVMArray)
        }
    }
    
    // Call api and set data
    func fetchData() {
        
        NetworkManager.sharedManager.callApiForGetJsonData(connectionURL: "datajson", WithJsonDictionary: [:], WithSuccessHandler:{ (dictObj, message) in
            print("Dict >>> \(dictObj)")
            self.getDataWithModel(dict: dictObj)
        }) { (errorMessage, urlString) in
            print("Fail")
        }
        
    }
    
    // Set data into Model
    func getDataWithModel(dict : Data){
        
        // let photoArray = dict["photos"] as! [[String : Any]]
        
        do {
            let objDecoder = JSONDecoder()
            //objDecoder.dateDecodingStrategy = .iso8601
            let objPartsPhoto  = try objDecoder.decode(PartsPhoto.self, from: dict)
            photoVMArray = objPartsPhoto.photos
        }
        catch(let error){
            print("JSON Parsing Error >> \(error)")
        }
        
        
        
        // Crate local Array to store the Photo Models.
        //        var tempArray = [photo]()
        //
        //        if photoArray.count > 0 {
        //
        //            for tempDict in photoArray {
        //
        //                var strName = ""
        //                var strDescription = ""
        //                var imageURL = ""
        //
        //                if let name = tempDict["name"] {
        //                    strName = name as! String
        //                }
        //
        //                if let description =  tempDict["description"] as? String {
        //                    strDescription = description
        //                }
        //
        //                if let imgURL = tempDict["image_url"] {
        //                    imageURL = imgURL as! String
        //                }
        //
        //                tempArray.append(photo(name: strName, descritpion: strDescription , imageURL:imageURL))
        //
        //            }
        
        // Set local Array to Photo Global Array
        // This time its call the
        //photoVMArray = tempArray
        
        //}
        
    }
    
    
    
}
