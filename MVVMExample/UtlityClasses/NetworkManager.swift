

import UIKit

class NetworkManager: NSObject {
    
    static let sharedManager = NetworkManager()
    
    // func callApiForGetJsonData(connectionURL url:String ,WithJsonDictionary jsonDict:[String:String] , WithSuccessHandler successHandler:@escaping(Dictionary<String ,Any>, String) -> Void, AndFailureHandler failureHandler: @escaping(String ,String) -> Void)
    
    func callApiForGetJsonData(connectionURL url:String ,WithJsonDictionary jsonDict:[String:String] , WithSuccessHandler successHandler:@escaping(Data, String) -> Void, AndFailureHandler failureHandler: @escaping(String ,String) -> Void)
    {
        if let url = Bundle.main.url(forResource: url, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dictData = object as? [String: Any] {
                    //successHandler(dictData, "Success")
                    successHandler(data, "Success")
                }
                else {
                    failureHandler("Fail","Fail")
                }
            } catch {
                print("Error!! Unable to parse  \("textjson").json")
            }
        }
        
    }
    
}
