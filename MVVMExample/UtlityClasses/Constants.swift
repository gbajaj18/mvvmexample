//
//  Constants.swift


struct key {
    
    
    
    static let k_Alert = "Alert"
    static let k_Ok = "Ok"
    
    // Message Key
    
    static let k_Msg_FirstName = "Please Enter First Name"
    static let k_Msg_LastName = "Please Enter Last Name"
    static let k_Msg_Email = "Please Enter Valid Email Address"
    static let k_Msg_Password = "Please Enter Password"
    static let k_Msg_PasswordRange = "Password should be greater then 8 and less then 20 characters"

    // Storyboard
    
    static let k_SB_Onboarding = "Onboarding"
    static let k_SB_Dashboard = "Dashboard"
    
    
}


